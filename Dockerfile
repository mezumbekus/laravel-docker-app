FROM php:7.3-fpm
COPY ./www /var/www
WORKDIR /var/www
USER root
RUN apt-get update && apt-get install -y \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6 \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    unzip \
    git \
    curl
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash - && apt-get install -y nodejs
RUN cp .env.example .env
RUN chown -R www:www .
USER www
RUN  composer update && \
    php artisan key:generate && \
    npm install

EXPOSE 9000
CMD ["php-fpm"]
